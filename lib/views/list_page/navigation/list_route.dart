import 'package:flutter/material.dart';

import '../list_page.dart';
import 'list_navigator.dart';

class ListRoute extends PageRouteBuilder {
  ListRoute()
      : super(
          pageBuilder: (context, animation1, animation2) =>
              ListPage(navigator: ListNavigator(context)),
          transitionDuration: Duration.zero,
          reverseTransitionDuration: Duration.zero,
        );
}
