import 'package:flutter/material.dart';
import 'package:test_incode/models/character/character_model.dart';
import 'package:test_incode/views/details_page/navigation/details_route.dart';
import 'package:test_incode/views/home_page/navigation/home_route.dart';

class ListNavigator {
  final NavigatorState _navigator;
  NavigatorState get navigator => _navigator;
  ListNavigator(BuildContext context) : _navigator = Navigator.of(context);

  void goToDetailPage(CharacterModel character, bool access) {
    navigator.push(DetailsRoute(character, access));
  }

  void goToHomePage() {
    navigator.pushAndRemoveUntil(HomeRoute(), (_) => false);
  }
}
