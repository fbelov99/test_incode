import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_incode/app/global_bloc/bloc/global_bloc.dart';
import 'package:test_incode/widget_helper.dart/history_item.dart';
import 'package:test_incode/widget_helper.dart/custom_scaffold.dart';

import '../../widget_helper.dart/count_buttons.dart';
import 'navigation/list_navigator.dart';

class ListPage extends StatefulWidget {
  final ListNavigator navigator;
  const ListPage({super.key, required this.navigator});

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  TextEditingController filter = TextEditingController();

  @override
  void dispose() {
    filter.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GlobalBloc, GlobalState>(builder: (context, state) {
      final bloc = context.read<GlobalBloc>();
      return GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: CustomScaffold(
          pageIndex: 1,
          bloc: bloc,
          body: state.isBusy
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CountButtons(state: state),
                        Padding(
                          padding: EdgeInsets.only(bottom: 20.h),
                          child: TextFormField(
                            controller: filter,
                            onChanged: (_) {
                              setState(() {});
                            },
                            decoration: InputDecoration(
                                hintText: 'Filter',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10.r))),
                          ),
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: state.history?.length ?? 0,
                            itemBuilder: (_, i) {
                              if (filter.text.isNotEmpty &&
                                      (state.history?[i].character?.name
                                              ?.toLowerCase()
                                              .contains(
                                                  filter.text.toLowerCase()) ??
                                          false) ||
                                  filter.text.isEmpty) {
                                return Padding(
                                  padding: EdgeInsets.only(bottom: 10.h),
                                  child: InkWell(
                                    onTap: () {
                                      if (state.history?[i].character != null) {
                                        widget.navigator.goToDetailPage(
                                            state.history![i].character!,
                                            state.history![i].isWin);
                                      }
                                    },
                                    child: HistoryItem(
                                        history: state.history?[i],
                                        onRetry: () {
                                          bloc.add(ChangeCurrentEvent(
                                              state.history?[i].character));
                                          widget.navigator.goToHomePage();
                                        }),
                                  ),
                                );
                              }
                              return Container();
                            }),
                      ],
                    ),
                  ),
                ),
        ),
      );
    });
  }
}
