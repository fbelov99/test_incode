import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_incode/app/global_bloc/bloc/global_bloc.dart';
import 'package:test_incode/res/assets.dart';
import 'package:test_incode/res/colors.dart';
import 'package:test_incode/res/styles.dart';
import 'package:test_incode/widget_helper.dart/button.dart';
import 'package:test_incode/widget_helper.dart/custom_scaffold.dart';

import '../../widget_helper.dart/count_buttons.dart';
import '../../widget_helper.dart/warp_indicator.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GlobalBloc, GlobalState>(builder: (context, state) {
      final bloc = context.read<GlobalBloc>();
      return CustomScaffold(
        pageIndex: 0,
        bloc: bloc,
        body: state.isBusy
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : WarpIndicator(
                onRefresh: () => Future.delayed(const Duration(seconds: 2), () {
                  bloc.add(const ChangeCurrentEvent(null));
                }),
                child: SingleChildScrollView(
                  child: Container(
                    color: CustomColors.white,
                    height: 720.h,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.w),
                      child: Column(
                        children: [
                          CountButtons(state: state),
                          if (state.currentChar?.image?.isNotEmpty ?? false)
                            SizedBox(
                                height: 150.h,
                                child: CachedNetworkImage(
                                    imageUrl: state.currentChar!.image!))
                          else
                            Icon(Icons.person, size: 150.r),
                          Padding(
                            padding: EdgeInsets.only(bottom: 15.h, top: 5.h),
                            child: Text(
                              state.currentChar?.name ?? '',
                              style: TextStyling.blackText(16, FontWeight.w700),
                            ),
                          ),
                          GridView.count(
                            shrinkWrap: true,
                            padding: EdgeInsets.zero,
                            crossAxisCount: 2,
                            semanticChildCount: 4,
                            childAspectRatio: 1.4,
                            mainAxisSpacing: 10.h,
                            crossAxisSpacing: 10.w,
                            physics: const NeverScrollableScrollPhysics(),
                            children: [
                              BestButton(
                                onTap: () => context
                                    .read<GlobalBloc>()
                                    .add(const CheckHouse('Gryffindor')),
                                height: 20.h,
                                width: 20.h,
                                leading: Assets.images.gryffindor
                                    .image(height: 60.h),
                                text: 'Gryffindor',
                              ),
                              BestButton(
                                onTap: () =>
                                    bloc.add(const CheckHouse('Slytherin')),
                                height: 20.h,
                                width: 20.h,
                                leading:
                                    Assets.images.slytherin.image(height: 60.h),
                                text: 'Slytherin',
                              ),
                              BestButton(
                                onTap: () =>
                                    bloc.add(const CheckHouse('Ravenclaw')),
                                height: 20.h,
                                width: 20.h,
                                leading:
                                    Assets.images.ravenclaw.image(height: 60.h),
                                text: 'Ravenclaw',
                              ),
                              BestButton(
                                onTap: () =>
                                    bloc.add(const CheckHouse('Hufflepuff')),
                                height: 20.h,
                                width: 20.h,
                                leading: Assets.images.hufflepuff
                                    .image(height: 60.h),
                                text: 'Hufflepuff',
                              )
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10.h),
                            child: BestButton(
                              onTap: () => bloc.add(const CheckHouse('')),
                              height: 81.h,
                              width: 400.w,
                              text: 'Not in a house',
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      );
    });
  }
}
