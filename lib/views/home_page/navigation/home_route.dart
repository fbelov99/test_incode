import 'package:flutter/material.dart';

import '../home_page.dart';

class HomeRoute extends PageRouteBuilder {
  HomeRoute()
      : super(
          pageBuilder: (context, animation1, animation2) => const HomePage(),
          transitionDuration: Duration.zero,
          reverseTransitionDuration: Duration.zero,
        );
}
