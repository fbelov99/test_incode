import 'package:flutter/material.dart';

import '../../list_page/navigation/list_route.dart';

class HomeNavigator {
  final NavigatorState _navigator;
  NavigatorState get navigator => _navigator;
  HomeNavigator(BuildContext context) : _navigator = Navigator.of(context);

  void goToListPage() {
    navigator.push(ListRoute());
  }
}
