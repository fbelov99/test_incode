import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_incode/res/assets.dart';
import 'package:test_incode/res/styles.dart';

import '../../models/character/character_model.dart';
import '../../res/colors.dart';
import 'navigation/details_navigator.dart';

class DetailsPage extends StatelessWidget {
  final CharacterModel character;
  final bool access;
  final DetailsNavigator navigator;
  const DetailsPage(
      {super.key,
      required this.navigator,
      required this.character,
      this.access = false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(character.name ?? 'No Name',
            style: TextStyling.blackText(18, FontWeight.w500)),
        leadingWidth: 80.w,
        leading: IconButton(
          icon: Row(
            children: [
              const Icon(
                Icons.arrow_back_ios_new_outlined,
                color: CustomColors.black,
              ),
              Text(
                'Back',
                style: TextStyling.blackText(12, FontWeight.w700),
              ),
            ],
          ),
          onPressed: Navigator.of(context).pop,
        ),
      ),
      extendBody: true,
      backgroundColor: CustomColors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (character.image?.isNotEmpty ?? false)
                  SizedBox(
                      height: 150.h,
                      child: CachedNetworkImage(imageUrl: character.image!))
                else
                  Icon(Icons.person, size: 150.r),
                Padding(
                  padding: EdgeInsets.only(left: 12.w),
                  child: SizedBox(
                    width: 180.w,
                    child: access
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              text(
                                  'House: ${(character.house?.isEmpty ?? true) ? 'Not in a house' : character.house}'),
                              if (character.dateOfBirth?.isNotEmpty ?? false)
                                text(
                                    'Date of birth: ${character.dateOfBirth ?? 'Unknown'}'),
                              if (character.actor?.isNotEmpty ?? false)
                                text('Actor: ${character.actor ?? 'Unknown'}'),
                              if (character.species?.isNotEmpty ?? false)
                                text(
                                    'Species: ${character.species ?? 'Unknown'}'),
                              if (character.ancestry?.isNotEmpty ?? false)
                                text(
                                    'Ancestry: ${character.ancestry ?? 'Unknown'}'),
                              if (character.patronus?.isNotEmpty ?? false)
                                text(
                                    'Patronus: ${character.patronus ?? 'Unknown'}'),
                              if (character.gender?.isNotEmpty ?? false)
                                text(
                                    'Gender: ${character.gender ?? 'Unknown'}'),
                              if (character.eyeColor?.isNotEmpty ?? false)
                                text(
                                    'Eye color: ${character.eyeColor ?? 'Unknown'}'),
                              if (character.hairColor?.isNotEmpty ?? false)
                                text(
                                    'Hair color: ${character.hairColor ?? 'Unknown'}'),
                              if (character.wand?.wood?.isNotEmpty ?? false)
                                text(
                                    'Wand: ${character.wand?.toJson().toString() ?? 'Unknown'}'),
                              if (character.alternateNames?.isNotEmpty ?? false)
                                text(
                                    'Alternate names: ${character.alternateNames?.join(', ') ?? 'Unknown'}'),
                              if (character.alternateActors?.isNotEmpty ??
                                  false)
                                text(
                                    'Alternate names: ${character.alternateActors?.join(', ') ?? 'Unknown'}'),
                              if (character.alive != null)
                                text('Alive: ${character.alive ?? 'Unknown'}'),
                              if (character.wizard != null)
                                text(
                                    'Wizard: ${character.wizard ?? 'Unknown'}'),
                              if (character.hogwartsStudent != null)
                                text(
                                    'Student: ${character.hogwartsStudent ?? 'Unknown'}'),
                              if (character.hogwartsStaff != null)
                                text(
                                    'Hogwarts staff: ${character.hogwartsStaff ?? 'Unknown'}'),
                            ],
                          )
                        : Assets.images.access.image(),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget text(String text) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5.h),
      child: Text(
        text,
        style: TextStyling.blackText(12, FontWeight.w400),
      ),
    );
  }
}
