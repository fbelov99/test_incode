import 'package:flutter/material.dart';

class DetailsNavigator {
  final NavigatorState _navigator;
  NavigatorState get navigator => _navigator;
  DetailsNavigator(BuildContext context) : _navigator = Navigator.of(context);

  void goToListPage() {
    // navigator.push(ListRoute());
  }
}
