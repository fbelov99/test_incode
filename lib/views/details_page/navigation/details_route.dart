import 'package:flutter/material.dart';
import 'package:test_incode/models/character/character_model.dart';

import '../details_page.dart';
import 'details_navigator.dart';

class DetailsRoute extends PageRouteBuilder {
  DetailsRoute(CharacterModel character, bool access)
      : super(
          pageBuilder: (context, animation1, animation2) => DetailsPage(
            navigator: DetailsNavigator(context),
            character: character,
            access: access,
          ),
          transitionDuration: Duration.zero,
          reverseTransitionDuration: Duration.zero,
        );
}
