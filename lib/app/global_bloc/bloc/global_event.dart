part of 'global_bloc.dart';

sealed class GlobalEvent extends Equatable {
  const GlobalEvent();

  @override
  List<Object> get props => [];
}

final class GlobalInitialEvent extends GlobalEvent {}

final class ChangeCurrentEvent extends GlobalEvent {
  final CharacterModel? currentChar;
  const ChangeCurrentEvent(this.currentChar);
}

final class CheckHouse extends GlobalEvent {
  final String house;
  const CheckHouse(this.house);
}

final class AddWin extends GlobalEvent {}

final class AddLoss extends GlobalEvent {}

final class ResetEvent extends GlobalEvent {}
