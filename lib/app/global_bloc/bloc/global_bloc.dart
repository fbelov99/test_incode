import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_incode/app/repository/global_repository.dart';
import 'package:test_incode/models/character/character_model.dart';
import 'package:test_incode/models/history_model.dart';

part 'global_event.dart';
part 'global_state.dart';

class GlobalBloc extends Bloc<GlobalEvent, GlobalState> {
  final GlobalRepository globalRepository;
  GlobalBloc({required this.globalRepository}) : super(const GlobalInitial()) {
    on<GlobalEvent>((event, emit) {});

    on<GlobalInitialEvent>(_getAllChar);

    on<ChangeCurrentEvent>((event, emit) {
      if (event.currentChar != null) {
        emit(state.copyWith(currentChar: event.currentChar));
      } else {
        _getRandomChar(emit);
      }
    });

    on<CheckHouse>(_checkHouse);

    on<AddWin>((event, emit) {
      List<HistoryModel>? history = _changeHistory(true);
      _getRandomChar(emit);
      emit(state.copyWith(
          win: state.win + 1, total: state.total + 1, history: history));
    });

    on<AddLoss>((event, emit) {
      List<HistoryModel>? history = _changeHistory(false);
      emit(state.copyWith(
          loss: state.loss + 1, total: state.total + 1, history: history));
    });

    on<ResetEvent>((event, emit) {
      emit(state.copyWith(isBusy: true));
      _getRandomChar(emit);
      emit(state.copyWith(
        total: 0,
        win: 0,
        loss: 0,
        history: [],
        isBusy: false,
      ));
    });
  }

  void _checkHouse(CheckHouse event, Emitter<GlobalState> emit) {
    emit(state.copyWith(isBusy: true));
    if (state.currentChar?.house == event.house) {
      add(AddWin());
    } else {
      add(AddLoss());
    }
    emit(state.copyWith(isBusy: false));
  }

  List<HistoryModel>? _changeHistory(bool isWin) {
    List<HistoryModel>? history = state.history?.toList();
    if (history?.any((e) => e.character?.id == state.currentChar?.id) ??
        false) {
      int attempts = history
              ?.firstWhere((e) => e.character?.id == state.currentChar?.id)
              .attempts ??
          1;
      history
        ?..removeWhere((e) => e.character?.id == state.currentChar?.id)
        ..add(HistoryModel(
            character: state.currentChar,
            isWin: isWin,
            attempts: attempts + 1));
    } else if (history?.isNotEmpty ?? false) {
      history?.add(HistoryModel(character: state.currentChar, isWin: isWin));
    } else {
      history = [HistoryModel(character: state.currentChar, isWin: isWin)];
    }
    return history;
  }

  void _getAllChar(GlobalInitialEvent event, Emitter<GlobalState> emit) async {
    emit(state.copyWith(isBusy: true));
    List<CharacterModel> allChar = await globalRepository.getAllCharacters();
    emit(state.copyWith(allChar: allChar, isBusy: false));
    _getRandomChar(emit);
  }

  void _getRandomChar(Emitter<GlobalState> emit) {
    List<CharacterModel>? allChar = state.allChar?.toList();
    if ((allChar?.isNotEmpty ?? false)) {
      int rnd = Random().nextInt(allChar!.length);
      CharacterModel? currentRnd = allChar.elementAt(rnd);
      emit(state.copyWith(currentChar: currentRnd));
    }
  }
}
