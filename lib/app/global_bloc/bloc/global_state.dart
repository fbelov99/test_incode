part of 'global_bloc.dart';

@immutable
sealed class GlobalState extends Equatable {
  final List<CharacterModel>? allChar;
  final List<HistoryModel>? history;
  final CharacterModel? currentChar;
  final int total, loss, win;
  final bool isBusy;

  const GlobalState(
      {this.allChar,
      this.history,
      this.currentChar,
      this.loss = 0,
      this.win = 0,
      this.total = 0,
      this.isBusy = false});

  GlobalState copyWith(
      {List<CharacterModel>? allChar,
      final List<HistoryModel>? history,
      CharacterModel? currentChar,
      int? total,
      int? loss,
      int? win,
      bool? isBusy}) {
    return GlobalInitial(
      allChar: allChar ?? this.allChar,
      history: history ?? this.history,
      currentChar: currentChar ?? this.currentChar,
      total: total ?? this.total,
      win: win ?? this.win,
      loss: loss ?? this.loss,
      isBusy: isBusy ?? this.isBusy,
    );
  }

  @override
  List<Object?> get props =>
      [allChar, history, currentChar, loss, win, total, isBusy];
}

final class GlobalInitial extends GlobalState {
  const GlobalInitial(
      {super.allChar,
      super.history,
      super.currentChar,
      super.loss,
      super.win,
      super.total,
      super.isBusy});
}
