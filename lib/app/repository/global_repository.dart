import 'package:flutter/material.dart';
import 'package:test_incode/models/character/character_model.dart';
import 'package:test_incode/services/api_service.dart';

class GlobalRepository {
  Future<List<CharacterModel>> getAllCharacters() async {
    try {
      List<CharacterModel> allChar = await ApiService().getAllCharacters();

      return allChar;
    } catch (err) {
      debugPrint(err.toString());
    }

    return [];
  }
}
