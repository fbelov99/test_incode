import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../views/home_page/home_page.dart';
import 'global_bloc/bloc/global_bloc.dart';
import 'repository/global_repository.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            lazy: false,
            create: (context) =>
                GlobalBloc(globalRepository: GlobalRepository())
                  ..add(GlobalInitialEvent())),
      ],
      child: ScreenUtilInit(
        designSize: const Size(375, 812),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp(
            builder: FToastBuilder(),
            debugShowCheckedModeBanner: false,
            title: 'TestIncode',
            home: const HomePage(),
          );
        },
      ),
    );
  }
}
