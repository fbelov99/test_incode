import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_incode/app/global_bloc/bloc/global_bloc.dart';

import '../res/styles.dart';
import 'button.dart';

class CountButtons extends StatelessWidget {
  final GlobalState state;
  const CountButtons({super.key, required this.state});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          BestButton(
            height: 60.h,
            width: 60.h,
            leading: Text(state.total.toString(),
                style: TextStyling.blackText(16, FontWeight.w700)),
            text: 'Total',
            fontWeight: FontWeight.w400,
            textSize: 12,
          ),
          BestButton(
            height: 60.h,
            width: 60.h,
            leading: Text(state.win.toString(),
                style: TextStyling.blackText(16, FontWeight.w700)),
            text: 'Success',
            fontWeight: FontWeight.w400,
            textSize: 12,
          ),
          BestButton(
            height: 60.h,
            width: 60.h,
            leading: Text(state.loss.toString(),
                style: TextStyling.blackText(16, FontWeight.w700)),
            text: 'Failed',
            fontWeight: FontWeight.w400,
            textSize: 12,
          ),
        ],
      ),
    );
  }
}
