import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:test_incode/models/history_model.dart';

import '../res/styles.dart';

class HistoryItem extends StatelessWidget {
  final HistoryModel? history;
  final VoidCallback? onRetry;
  const HistoryItem({super.key, required this.history, this.onRetry});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              if (history?.character?.image?.isNotEmpty ?? false)
                SizedBox(
                    height: 30.h,
                    child: CachedNetworkImage(
                        imageUrl: history!.character!.image!))
              else
                Icon(Icons.person, size: 30.r),
              Padding(
                padding: EdgeInsets.only(left: 10.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      history?.character?.name ?? 'No Name',
                      style: TextStyling.blackText(15, FontWeight.w400),
                    ),
                    Text(
                      'Attempts: ${history?.attempts}',
                      style: TextStyling.blackText(10, FontWeight.w400),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            children: [
              if (!(history?.isWin ?? true))
                IconButton(onPressed: onRetry, icon: const Icon(Icons.refresh)),
              if (history?.isWin ?? false)
                const Icon(
                  Icons.check_circle_outline_outlined,
                  color: Colors.green,
                )
              else
                const Icon(
                  Icons.close,
                  color: Colors.red,
                ),
            ],
          ),
        ],
      ),
    );
  }
}
