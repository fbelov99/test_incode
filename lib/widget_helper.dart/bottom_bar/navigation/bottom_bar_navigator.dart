import 'package:flutter/material.dart';

import '../../../views/home_page/navigation/home_route.dart';
import '../../../views/list_page/navigation/list_route.dart';

class BottomBarNavigator {
  final NavigatorState _navigator;
  NavigatorState get navigator => _navigator;
  BottomBarNavigator(BuildContext context) : _navigator = Navigator.of(context);

  void goToHomePage() {
    navigator.pushAndRemoveUntil(HomeRoute(), (route) => false);
  }

  void goToListPage() {
    navigator.pushAndRemoveUntil(ListRoute(), (route) => false);
  }
}
