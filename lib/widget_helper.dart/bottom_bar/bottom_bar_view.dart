import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../res/colors.dart';
import 'navigation/bottom_bar_navigator.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    super.key,
    required this.pageIndex,
    required this.navigator,
  });

  final int pageIndex;
  final BottomBarNavigator navigator;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(52, 43, 37, 0.15),
            blurRadius: 8,
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.r),
          topRight: Radius.circular(12.r),
        ),
        child: BottomAppBar(
          height: 64.h,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 50.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: Icon(Icons.home,
                      size: 35.r,
                      color: pageIndex == 0
                          ? CustomColors.primary
                          : CustomColors.secondary),
                  onPressed: navigator.goToHomePage,
                ),
                IconButton(
                  icon: Icon(Icons.list,
                      size: 35.r,
                      color: pageIndex == 1
                          ? CustomColors.primary
                          : CustomColors.secondary),
                  onPressed: navigator.goToListPage,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
