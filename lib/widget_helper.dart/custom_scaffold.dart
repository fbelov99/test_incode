import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../app/global_bloc/bloc/global_bloc.dart';
import '../res/colors.dart';
import '../res/styles.dart';
import 'bottom_bar/bottom_bar_view.dart';
import 'bottom_bar/navigation/bottom_bar_navigator.dart';

class CustomScaffold extends StatelessWidget {
  const CustomScaffold(
      {super.key,
      required this.pageIndex,
      this.body,
      this.backgroundColor,
      this.isBottomBar = true,
      this.resizeToAvoidBottomInset = false,
      this.refreshCallback,
      required this.bloc});
  final int pageIndex;
  final Widget? body;
  final GlobalBloc bloc;
  final Color? backgroundColor;
  final bool isBottomBar;
  final bool resizeToAvoidBottomInset;
  final VoidCallback? refreshCallback;

  @override
  Widget build(BuildContext context) {
    final navigator = BottomBarNavigator(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('List Screen',
            style: TextStyling.blackText(18, FontWeight.w500)),
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 20.w),
            child: InkWell(
                onTap: () => bloc.add(ResetEvent()),
                child: Text(
                  'Reset',
                  style: TextStyling.blackText(12, FontWeight.w400),
                )),
          )
        ],
      ),
      extendBody: true,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      backgroundColor: backgroundColor ?? CustomColors.white,
      body: Padding(
        padding: EdgeInsets.only(top: 12.h),
        child: SafeArea(bottom: false, child: body ?? const SizedBox.shrink()),
      ),
      bottomNavigationBar: isBottomBar
          ? BottomBar(pageIndex: pageIndex, navigator: navigator)
          : null,
    );
  }
}
