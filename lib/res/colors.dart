import 'package:flutter/material.dart';

class CustomColors {
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF000000);
  static const Color primary = Color(0xFF82CB46);
  static const Color secondary = Color(0xFF5A5653);
}
