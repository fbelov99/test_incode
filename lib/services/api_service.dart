import 'dart:convert';

import 'package:http/http.dart';
import 'package:test_incode/models/character/character_model.dart';

class ApiService {
  ApiService({
    Client? httpClient,
    this.baseUrl = 'https://hp-api.onrender.com/api',
  }) : _httpClient = httpClient ?? Client();

  final String baseUrl;
  final Client _httpClient;

  Uri getUrl({
    required String url,
    Map<String, String>? extraParameters,
  }) {
    return Uri.parse('$baseUrl/$url').replace(queryParameters: extraParameters);
  }

  Future<List<CharacterModel>> getAllCharacters() async {
    try {
      final response = await _httpClient.get(
        getUrl(
          url: 'characters',
        ),
      );

      return List.of(jsonDecode(response.body) ?? [])
          .map((e) => CharacterModel.fromJson(e))
          .toList();
    } catch (e) {
      throw e.toString();
    }
  }
}
