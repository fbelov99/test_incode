import 'package:test_incode/models/character/character_model.dart';

class HistoryModel {
  final CharacterModel? character;
  final bool isWin;
  final int attempts;
  const HistoryModel({this.character, this.isWin = false, this.attempts = 1});
}
