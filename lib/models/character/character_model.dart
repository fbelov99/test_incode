import 'package:json_annotation/json_annotation.dart';
import 'package:test_incode/models/wand/wand_model.dart';

part 'character_model.g.dart';

@JsonSerializable()
class CharacterModel {
  CharacterModel({
    this.id,
    this.name,
    this.actor,
    this.alive,
    this.alternateActors,
    this.alternateNames,
    this.ancestry,
    this.dateOfBirth,
    this.eyeColor,
    this.gender,
    this.hairColor,
    this.hogwartsStaff,
    this.hogwartsStudent,
    this.house,
    this.image,
    this.patronus,
    this.species,
    this.wand,
    this.wizard,
    this.yearOfBirth,
  });

  final String? id,
      name,
      species,
      gender,
      house,
      ancestry,
      eyeColor,
      hairColor,
      patronus,
      actor,
      image,
      dateOfBirth;
  final double? yearOfBirth;
  final bool? alive, wizard, hogwartsStudent, hogwartsStaff;
  @JsonKey(name: 'alternate_names')
  final List<String>? alternateNames;
  @JsonKey(name: 'alternate_actors')
  final List<String>? alternateActors;
  final WandModel? wand;

  factory CharacterModel.fromJson(Map<String, dynamic> json) =>
      _$CharacterModelFromJson(json);

  Map<String, dynamic> toJson() => _$CharacterModelToJson(this);
}
