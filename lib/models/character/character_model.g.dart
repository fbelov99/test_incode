// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'character_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CharacterModel _$CharacterModelFromJson(Map<String, dynamic> json) =>
    CharacterModel(
      id: json['id'] as String?,
      name: json['name'] as String?,
      actor: json['actor'] as String?,
      alive: json['alive'] as bool?,
      alternateActors: (json['alternate_actors'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      alternateNames: (json['alternate_names'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      ancestry: json['ancestry'] as String?,
      dateOfBirth: json['dateOfBirth'] as String?,
      eyeColor: json['eyeColor'] as String?,
      gender: json['gender'] as String?,
      hairColor: json['hairColor'] as String?,
      hogwartsStaff: json['hogwartsStaff'] as bool?,
      hogwartsStudent: json['hogwartsStudent'] as bool?,
      house: json['house'] as String?,
      image: json['image'] as String?,
      patronus: json['patronus'] as String?,
      species: json['species'] as String?,
      wand: json['wand'] == null
          ? null
          : WandModel.fromJson(json['wand'] as Map<String, dynamic>),
      wizard: json['wizard'] as bool?,
      yearOfBirth: (json['yearOfBirth'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$CharacterModelToJson(CharacterModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'species': instance.species,
      'gender': instance.gender,
      'house': instance.house,
      'ancestry': instance.ancestry,
      'eyeColor': instance.eyeColor,
      'hairColor': instance.hairColor,
      'patronus': instance.patronus,
      'actor': instance.actor,
      'image': instance.image,
      'dateOfBirth': instance.dateOfBirth,
      'yearOfBirth': instance.yearOfBirth,
      'alive': instance.alive,
      'wizard': instance.wizard,
      'hogwartsStudent': instance.hogwartsStudent,
      'hogwartsStaff': instance.hogwartsStaff,
      'alternate_names': instance.alternateNames,
      'alternate_actors': instance.alternateActors,
      'wand': instance.wand,
    };
