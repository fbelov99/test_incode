import 'package:json_annotation/json_annotation.dart';

part 'wand_model.g.dart';

@JsonSerializable()
class WandModel {
  WandModel({
    this.wood,
    this.core,
    this.length,
  });

  final String? wood, core;
  final double? length;

  factory WandModel.fromJson(Map<String, dynamic> json) =>
      _$WandModelFromJson(json);

  Map<String, dynamic> toJson() => _$WandModelToJson(this);
}
